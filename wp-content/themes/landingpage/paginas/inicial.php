<?php
/**
 * Template Name: Inicial
 * Description: Página inicial
 *
 * @package landingpage
 */


$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];
get_header(); ?>
	
 <div class="pg-inicial">
      <div class="container">
        <div class="row">
          <div class="col-sm-7">
            <figure>
              <img src="<?php echo $foto  ?>" alt="">
            </figure>

            <article>
              <?php 
                while ( have_posts() ) : the_post();
                  echo the_content();
                endwhile;
              ?>
              <a href="https://www.gebaby.com.br/" target="_blank">Venha nos conhecer!</a>
            </article>
          </div>
          <div class="col-sm-5">
            <div class="formulario">
              <div class="form">
                  <?php //echo do_shortcode('[contact-form-7 id="10" title="Contact form 1"]'); ?>

                  <!--START Scripts : this is the script part you can add to the header of your theme-->
                  <script type="text/javascript" src="<?php echo get_home_url()  ?>/wp-includes/js/jquery/jquery.js?ver=2.8.1"></script>
                  <script type="text/javascript" src="<?php echo get_home_url()  ?>/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.8.1"></script>
                  <script type="text/javascript" src="<?php echo get_home_url()  ?>/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.8.1"></script>
                  <script type="text/javascript" src="<?php echo get_home_url()  ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.1"></script>
                  <script type="text/javascript">
                  /* <![CDATA[ */
                  var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"<?php echo get_home_url()  ?>/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
                  /* ]]> */
                  </script><script type="text/javascript" src="<?php echo get_home_url()  ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.1"></script>
                  <!--END Scripts-->
               

                  <div class="widget_wysija_cont html_wysija">
                    <div id="msg-form-wysija-html5a71f804afd69-2" class="wysija-msg ajax"></div>
                    <form id="form-wysija-html5a71f804afd69-2" method="post" action="#wysija" class="widget_wysija html_wysija">

                      <!-- INPUT NOME -->
                      <label>Nome completo <span class="wysija-required">*</span></label>
                      <input type="text" name="wysija[user][firstname]" class="validate[required]" title="Nome completo"  value="" />
                      <span class="abs-req">
                        <input type="text" name="wysija[user][abs][firstname]" class="validated[abs][firstname]" value="" />
                      </span>
                   
                      <!-- INPUT E-MAIL -->
                      <label>E-mail <span class="wysija-required">*</span></label>
                      <input type="text" name="wysija[user][email]" class="validate[required,custom[email]]" title="Email"  value="" />
                      <span class="abs-req">
                        <input type="text" name="wysija[user][abs][email]" class="validated[abs][email]" value="" />
                      </span>

                      <!-- INPUT ANIVERSÁRIO -->
                      <label for="">Aniversário</label>
                      <div class="row dataAniversario">
                        <div class="col-xs-4">
                        
                          <select class="wysija_date_month " name="wysija[field][cf_1][month]" placeholder="Mês">
                            <option value="1">Janeiro</option>
                            <option value="2">Fevereiro</option>
                            <option value="3">Março</option>
                            <option value="4">Abril</option>
                            <option value="5">Maio</option>
                            <option value="6">Junho</option>
                            <option value="7">Julho</option>
                            <option value="8">Agosto</option>
                            <option value="9">Setembro</option>
                            <option value="10">Outubro</option>
                            <option value="11">Novembro</option>
                            <option value="12">Dezembro</option>
                          </select>
                        </div>
                        <div class="col-xs-4">
                          <select class="wysija_date_day " name="wysija[field][cf_1][day]" placeholder="Dia">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                          </select>
                        </div>
                        <div class="col-xs-4">
                          <select class="wysija_date_year " name="wysija[field][cf_1][year]" placeholder="Ano">

                            <option value="2018">2018</option>

                            <option value="2017">2017</option>

                            <option value="2016">2016</option>

                            <option value="2015">2015</option>

                            <option value="2014">2014</option>

                            <option value="2013">2013</option>

                            <option value="2012">2012</option>

                            <option value="2011">2011</option>

                            <option value="2010">2010</option>

                            <option value="2009">2009</option>

                            <option value="2008">2008</option>

                            <option value="2007">2007</option>

                            <option value="2006">2006</option>

                            <option value="2005">2005</option>

                            <option value="2004">2004</option>

                            <option value="2003">2003</option>

                            <option value="2002">2002</option>

                            <option value="2001">2001</option>

                            <option value="2000">2000</option>

                            <option value="1999">1999</option>

                            <option value="1998">1998</option>

                            <option value="1997">1997</option>

                            <option value="1996">1996</option>

                            <option value="1995">1995</option>

                            <option value="1994">1994</option>

                            <option value="1993">1993</option>

                            <option value="1992">1992</option>

                            <option value="1991">1991</option>

                            <option value="1990">1990</option>

                            <option value="1989">1989</option>

                            <option value="1988">1988</option>

                            <option value="1987">1987</option>

                            <option value="1986">1986</option>

                            <option value="1985">1985</option>

                            <option value="1984">1984</option>

                            <option value="1983">1983</option>

                            <option value="1982">1982</option>

                            <option value="1981">1981</option>

                            <option value="1980">1980</option>

                            <option value="1979">1979</option>

                            <option value="1978">1978</option>

                            <option value="1977">1977</option>

                            <option value="1976">1976</option>

                            <option value="1975">1975</option>

                            <option value="1974">1974</option>

                            <option value="1973">1973</option>

                            <option value="1972">1972</option>

                            <option value="1971">1971</option>

                            <option value="1970">1970</option>

                            <option value="1969">1969</option>

                            <option value="1968">1968</option>

                            <option value="1967">1967</option>

                            <option value="1966">1966</option>

                            <option value="1965">1965</option>

                            <option value="1964">1964</option>

                            <option value="1963">1963</option>

                            <option value="1962">1962</option>

                            <option value="1961">1961</option>

                            <option value="1960">1960</option>

                            <option value="1959">1959</option>

                            <option value="1958">1958</option>

                            <option value="1957">1957</option>

                            <option value="1956">1956</option>

                            <option value="1955">1955</option>

                            <option value="1954">1954</option>

                            <option value="1953">1953</option>

                            <option value="1952">1952</option>

                            <option value="1951">1951</option>

                            <option value="1950">1950</option>

                            <option value="1949">1949</option>

                            <option value="1948">1948</option>

                            <option value="1947">1947</option>

                            <option value="1946">1946</option>

                            <option value="1945">1945</option>

                            <option value="1944">1944</option>

                            <option value="1943">1943</option>

                            <option value="1942">1942</option>

                            <option value="1941">1941</option>

                            <option value="1940">1940</option>

                            <option value="1939">1939</option>

                            <option value="1938">1938</option>

                            <option value="1937">1937</option>

                            <option value="1936">1936</option>

                            <option value="1935">1935</option>

                            <option value="1934">1934</option>

                            <option value="1933">1933</option>

                            <option value="1932">1932</option>

                            <option value="1931">1931</option>

                            <option value="1930">1930</option>

                            <option value="1929">1929</option>

                            <option value="1928">1928</option>

                            <option value="1927">1927</option>

                            <option value="1926">1926</option>

                            <option value="1925">1925</option>

                            <option value="1924">1924</option>

                            <option value="1923">1923</option>

                            <option value="1922">1922</option>

                            <option value="1921">1921</option>

                            <option value="1920">1920</option>

                            <option value="1919">1919</option>

                            <option value="1918">1918</option>

                          </select>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-6">
                          <label for="gravida">Está Grávida?</label>
                          <div class="row">
                            <div class="col-xs-6">
                              <div class="ckeck">
                                <input type="radio" class="wysija-radio" name="wysija[field][cf_7]" value="Sim"  />Sim
                                <span></span>
                              </div>
                            </div>
                            <div class="col-xs-6">
                              <div class="ckeck">
                                <input type="radio" class="wysija-radio " name="wysija[field][cf_7]" value="Não"  />Não
                                <span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-6">
                          <label for="">Quantos meses?</label>
                          <input type="text" name="wysija[field][cf_3]" class="validate[required,custom[onlyNumberSp]]" title="Quantos meses?"  value="" />
                          <span class="abs-req">
                            <input type="text" name="wysija[field][abs][cf_3]" class="validated[abs][cf_3]" value="" />
                          </span>
                        </div>
                      </div>
                     
                      <div class="row">
                        <div class="col-xs-6">
                          <label for="">Seu bebê já nasceu?</label>
                          <div class="row">
                            <div class="col-xs-6">
                              <div class="ckeck">
                                <input type="radio" class="validate[required]" name="wysija[field][cf_8]" value="Sim"  />Sim
                                <span></span>
                              </div>
                            </div>
                            <div class="col-xs-6">
                              <div class="ckeck">
                                <input type="radio" class="wysija-radio validate[required]" name="wysija[field][cf_8]" value="Não"  />Não
                                <span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-6">
                          <label for="">Quantos meses?</label>
                          <input type="text" name="wysija[field][cf_4]" class="validate[custom[onlyNumberSp]]" title="Quantos meses?"  value="" />
                          <span class="abs-req">
                            <input type="text" name="wysija[field][abs][cf_4]" class="validated[abs][cf_4]" value="" />
                          </span>
                        </div>
                      </div>
                      
                      <input class="wysija-submit wysija-submit-field" type="submit" value="Participe!" />
                      <input type="hidden" name="form_id" value="2" />
                      <input type="hidden" name="action" value="save" />
                      <input type="hidden" name="controller" value="subscribers" />
                      <input type="hidden" value="1" name="wysija-page" />
                      <input type="hidden" name="wysija[user_list][list_ids]" value="1" />
                    </form>
                </div>
              </div>
            </div>
            <img src="<?php bloginfo('template_directory'); ?>/img/imgFooter.png" alt="Rodape" class="ornamento">
          </div>
        </div>
      </div>
    </div>
        
<?php //get_footer(); ?>