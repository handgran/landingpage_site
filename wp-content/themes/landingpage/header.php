<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package landingpage
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<!-- TOPO -->
	<header class="topo">
		<div class="containerFull">
			<div class="logo topoLogo">
				<img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="">
			</div>
			<div class="row">
				<div class="col-sm-7 col-sm-12">
					<div class="texto">
						<p>Sorteio Kit mala</p>
						<p>maternidade, <span>participe!</span></p>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="logo topoLogohidden">
						<img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</header>


	<div class="modalContatoSucesso" id="modalContatoSucesso">
		<img src="<?php bloginfo('template_directory'); ?>/img/modal.png" alt="">
	</div>

  <script>




  </script>

  	<script>
		$(document).ready(function() {
			
			$('.wysija-submit.wysija-submit-field').click(function(){
				function show_popup(){
					if($(".updated").length >= 1) {
						$("#modalContatoSucesso").show("slow");
						clearInterval(robo);
						carregar();
					}
				};

				var robo = setInterval( show_popup, 500 );


				function carregar(){
					setTimeout(function(){ 
						window.location='https://www.gebaby.com.br/';
					}, 9000);
				}
			});

			$('#modalContatoSucesso').click(function(){
				$("#modalContatoSucesso").slideUp();
				setTimeout(function(){ 
						window.location='https://www.gebaby.com.br/';
					}, 1000);
			});

		});
	</script>